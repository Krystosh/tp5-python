#---------------------------------------
# Exemple de scores
#---------------------------------------
scores=[352100,325410,312785,220199,127853]
joueurs=['Batman','Robin','Batman','Joker','Batman']

#exo4.1
def meilleurs_score(nom) :
    """[Recherche le meilleure score d'un joueur passer en paramètre]

    Args:
        nom ([str]): [Un nom]

    Returns:
        [int]: [Retourne le meilleur score du nom passer en parametre et None s'il n'existe pas]
    """    
    score_maxi = None #Prend le score maxi du nom passer en parametre
    for i in range(len(joueurs)) :
        if joueurs[i] == nom :
            if score_maxi == None : 
              score_maxi = scores[i]
            elif score_maxi<scores[i] :
                score_maxi = scores[i]
    return score_maxi

def test_meilleurs_score() :
    assert meilleurs_score('Batman') == 352100
    assert meilleurs_score('') == None
    assert meilleurs_score('Maxime') == None 

#exo4.2
def croissant(liste_score) :
    """[Vérifie si la liste est bien triee dans l'ordre décroissant]

    Args:
        liste_score ([list]): [Liste de score]

    Returns:
        [bool]: [True si la liste est bien triee dans l'ordre décroissant et False sinon]
    """    
    for i in range(len(liste_score)-1) :
        if liste_score[i] < liste_score[i+1] :
            return False
    return True

def test_croissant() :
    assert croissant([]) == True
    assert croissant(scores) == True
    assert croissant([352100,325410,312785,22019900,127853]) == False

#exo4.3
def cb_meilleurs_joueur(nom) :
    """[summary]

    Args:
        nom ([type]): [description]

    Returns:
        [type]: [description]
    """    
    cpt = None
    for player in joueurs :
        if cpt == None :
            if player == nom :
                cpt = 1
        elif player == nom :
            cpt += 1
    return cpt 


def test_cb_meilleurs_joueur() :
    assert cb_meilleurs_joueur('Batman') == 3
    assert cb_meilleurs_joueur('') == None
    assert cb_meilleurs_joueur('Imad') == None


#exo4.4
def premiere_app(nom) :
    """[Recherche la premiere apparition d'un nom donner]

    Args:
        nom ([str]): [Un nom]

    Returns:
        [int]: [Retourne le classement ou la premiere apparition du nom apparait retourne None s'il n'existe pas]
    """    
    for i in range(len(scores)) :
        if joueurs[i] == nom :
            return i+1
    return None

def test_premiere_app() :
    assert premiere_app('Batman') == 1
    assert premiere_app('Oublie Imad') == None
    assert premiere_app('Robin') == 2


def ajout_liste(liste_score,liste_ajout) :
    for i in range(len(liste_score)) :
        for j in range(len(liste_ajout)) :
            if liste_score[i] < liste_ajout[j] :
                