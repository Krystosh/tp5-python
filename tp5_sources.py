def indice_quatrieme_val(liste,valeur):
    """[fonction qui permet de savoir à quel indice on rencontre la 4eme fois la valeur de val]

    Args:
        liste ([list]): [une liste de nombre]
        valeur ([int]): [le nombre que l'on souhaite chercher]

    Returns:
        [int]: [retourne l'indice au quel on a rencontrer val 4 fois]
    """
    cpt=0 #compte le nb de valeur déja énumérer
    cpt_eg_val=0 #
    for i in range(len(liste)):
        if liste[i]==valeur:
            cpt_eg_val+=1
            if cpt_eg_val>3:
                return cpt
        cpt+=1
    return None 

def test_indice_quatrieme_val() :
    assert indice_quatrieme_val([9,20,20,5,9,48,12,418,9,12,12,9],9) == 11
    assert indice_quatrieme_val([46,46,46,5,8,46,12,418,20,12,12,87],46) == 5
    assert indice_quatrieme_val([],20) == None
    assert indice_quatrieme_val([8,20,12,87],8) == None

def first_index_int (phrase) :
    """[Fonction qui permet de connaitre ou le premier chiffre apparait]

    Args:
        phrase ([str]): [Une phrase]

    Returns:
        [int]: [retourne l'index ou se situe le premier chiffre]
    """    
    chiffre = "0123456789"
    res = None
    for i in range(len(phrase)) :
        if phrase[i] in chiffre and res == None :
            res = i
    return res

def test_first_index_int () :
    assert first_index_int("on est le 30/09/2021") == 10
    assert first_index_int("Salut toi") == None
    assert first_index_int("") == None
    assert first_index_int("on est pas le 30/09/2021") == 14


# --------------------------------------
# Exemple de villes avec leur population
# --------------------------------------
liste_villes=["Blois","Bourges","Chartres","Châteauroux","Dreux","Joué-lès-Tours","Olivet","Orléans","Tours","Vierzon"]
population=[45871, 64668,  38426, 43442, 30664, 38250, 22168, 116238, 136463,  25725]

def nb_hab_ville (ville) :
    """[Renvois le nb de population d'une ville]

    Args:
        ville ([str]): [Une ville]

    Returns:
        [int]: [Retourne la population de la ville passer en paramètre]
    """    
    for i in range(len(liste_villes)) :
        if liste_villes[i] == ville:
            return population[i]
    return None

def test_nb_hab_ville () :
    assert nb_hab_ville('Blois') == 45871
    assert nb_hab_ville('') == None
    assert nb_hab_ville('Bourges') == 64668

    


def liste_triee (liste) :
    """[Fonction qui permet de triee une liste]

    Args:
        liste ([list]): [une liste de nombre]

    Returns:
        [list]: [retourne une liste de nb]
    """    
    liste_tri = []
    mini = liste[0]
    jmax = 0
    for i in range(len(liste)) :
        mini = liste[0]
        for j in range(len(liste)) :
            if liste[j] < mini :
                mini= liste[j]
                jmax = j
        liste_tri.append(mini)
        liste.pop(jmax)
        jmax=0
    return liste_tri

def is_triee (liste) :
    for i in range(1,len(liste)) :
        if liste[i]<liste[i-1] :
            return False
    return True


def test_is_triee () :
    assert is_triee([1,2,3,4,5,6]) == True
    assert is_triee([]) == True
    assert is_triee([2,1,5,6,7]) == False


def somme_depasser (liste,seuil) :
    """[Fait la somme des nombre de la liste et verifie sil depasse le seuil]

    Args:
        liste ([list]): [liste de nombre]
        seuil ([int]): [Un seuil]

    Returns:
        [bool]: [retourne true si la somme depasse le seuil et depasse false sil le depasse pas]
    """
    somme = 0
    for i in range(len(liste)) :
        somme+=liste[i]
        if somme > seuil :
            return True
    return False

def test_somme_depasser() :
    assert somme_depasser([1,2,3,1], 12) == False
    assert somme_depasser([], 0) == False
    assert somme_depasser([1],0 ) == True
    assert somme_depasser([1,456,12,465], 123) == True

def adresse_mail(chaine) :
    mot = ''
    nb_arobase = 0
    nb_point = 0
    repaire = 0
    if chaine[0] == '@' :
        return False
    if chaine[len(chaine)-1] == '.':
        return False
    for i in range(len(chaine)) :
        if chaine[i] == ' ' :
            return False
        elif chaine[i] == '@' :
            nb_arobase +=1
            repaire = i
        elif nb_arobase > 1 :
            return False
        elif chaine[i] == '.'  :
            nb_point +=1
        elif nb_arobase > 1 and i>repaire and nb_arobase != 0 :
            return False
    return True
print(adresse_mail('abdel.i@.com'))

def test_adresse_mail() :
    assert adresse_mail('abdel.elakrouti@.com') == True  
    assert adresse_mail('abdel.elakro uti@.com') == False  
    assert adresse_mail('abdel@.elakrouti@.com') == False  
    assert adresse_mail('@abdel.elakrouti.com') == False 


